﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOLID.InterfaceSegregationPrinciple
{
    /// <summary>
    /// This program demonstrate Interface Segregation Principle
    /// Which says: Shows only those methods to client which they need..
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            List<Customer> obj = new List<Customer>();
            obj.Add(new GoldCustomer());
            obj.Add(new SilverCustomer());

            // 1000 Old Clients
            ICustomer Iold = new Customer();
            Iold.Add();

            // New Clients
            IRead Inew = new Customer();
            Inew.Add();
            Inew.Read();

            //obj.Add(new InquiryCustomer()); // Now InquiryCustomer is not a type of Customer so if un comment this line we will get error

            foreach (Customer customer in obj)
            {
                customer.Add();
            }

        }
    }

    /// <summary>
    /// Interface for Inquiry customers
    /// </summary>
    public interface IInquiry
    {
        double CalculateDiscount();
    }

    /// <summary>
    /// Interface for actual Customers
    /// </summary>
    public interface ICustomer : IInquiry
    {
        void Add();
    }

    public interface IRead : ICustomer
    {
        void Read();
    }

    /// <summary>
    /// Customer class
    /// </summary>
    public class Customer : ICustomer, IRead // Simple Closed Customer Class
    {

        #region Properties

        //private int _customerType;

        //public int CustmerType
        //{
        //    get { return _customerType; }
        //    set { _customerType = value; }
        //}

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public Customer()
        {

        }

        /// <summary>
        /// Calculates Discount
        /// Method has been marked as Virtual in order to keep it open for modification in other class
        /// </summary>
        /// <returns></returns>
        public virtual double CalculateDiscount()
        {
            //Commented after step 5 below
            //if (CustmerType == 1) // Gold Customer
            //{
            //    return 10;
            //}
            //else // Silver Customer
            //{
            //    return 5;
            //}

            //1. Here the problem is maintaining this class, if tomorrow we need to add bronze customer, we will need to modify the if condition here.
            //2. To Solve this problem we need to use "Extension Class" this class will help use to maintain customerType in this way we wont require to modify customer class each time. 
            //3. This is how we apply OCP
            //4. i.e. Close This Customer Class for Modification but it can be extended. 
            //5. Modification doesn't mean fixing bugs, modification here means adding new functionality (like adding new customer type)
            // lets comment above if-else block


            return 0;


        }

        /// <summary>
        /// Add Customer
        /// </summary>
        public virtual void Add()
        {
            try
            {
                // Adds the customer to database...
                Console.WriteLine("Record Added to database");
            }
            catch (Exception ex)
            {
                //here customer class is doing extra task of writing logs which should be the responsibility of logger class
                // To solve this problem and to follow SRP, we need to create another class which has responsibility of logging

                //System.IO.File.WriteAllText(@"C:\Error.txt",ex.ToString());

                var obj = new ErrorHandler();
                obj.HandleError(ex.ToString());
            }

        }




        public void Read()
        {
            Console.WriteLine("Read record sucsessful...");
        }
    }



    /// <summary>
    /// Extension of Customer class using inheritance
    /// </summary>
    public class GoldCustomer : Customer // this class is open for extension
    {
        public override double CalculateDiscount()
        {
            return base.CalculateDiscount() + 10;
        }
    }

    /// <summary>
    /// Extension of Customer class using inheritance
    /// </summary>
    public class SilverCustomer : Customer
    {
        public override double CalculateDiscount()
        {
            return base.CalculateDiscount() + 5;

        }
    }

    /// <summary>
    /// Extension of Customer class using inheritance
    /// </summary>
    public class InquiryCustomer : IInquiry
    {
        public double CalculateDiscount()
        {
            return 2;
        }
    }

    /// <summary>
    /// This class handle error handling
    /// </summary>
    public class ErrorHandler
    {
        /// <summary>
        /// 
        /// </summary>
        public ErrorHandler()
        {

        }

        /// <summary>
        /// This method now handles error
        /// </summary>
        /// <param name="msg"></param>
        public void HandleError(string msg)
        {
            System.IO.File.WriteAllText(@"C:\Error.txt", msg);
        }

    }


}
