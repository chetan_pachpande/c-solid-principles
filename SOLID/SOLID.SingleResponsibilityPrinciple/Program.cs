﻿using System;

namespace SOLID.SingleResponsibilityPrinciple
{

    //SRP: Single Responsibility Principle
    //A Class Should Have Only One Responsibility and Not Multiple
   
    /// <summary>
    /// Entry Point of Class
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var custPayRollDep = new Customer();
            custPayRollDep.Add();
        }
    }

    #region Customer Class

    /// <summary>
    /// Customer class
    /// Responsibility: Store Customer Data and Add it to database
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Customer Constructor
        /// </summary>
        public Customer()
        {
            Console.WriteLine("Hello From Customer Class");
        }

        /// <summary>
        /// Add Customer
        /// </summary>
        public void Add()
        {
            try
            {
                // Adds the customer to database...
            }
            catch (Exception ex)
            {
                //BEFORE
                //here customer class is doing extra task of writing logs which should be the responsibility of logger class
                // To solve this problem and to follow SRP, we need to create another class which has responsibility of logging


                //System.IO.File.WriteAllText(@"C:\Error.txt",ex.ToString());

                //AFTER
                var obj = new ErrorHandler();
                obj.HandleError(ex.ToString());
            }
        }
    } 
    #endregion
    
    #region ErrorHandler Class
    
    /// <summary>
    /// ErrorHandler Class
    /// Responsibility: Handles errors
    /// </summary>
    public class ErrorHandler
    {
        /// <summary>
        /// Error Handler Constructor
        /// </summary>
        public ErrorHandler()
        {
            Console.WriteLine("Hello from ErrorHandler Class");
        }

        /// <summary>
        /// This method now handles error
        /// </summary>
        /// <param name="msg">Error Description</param>
        public void HandleError(string msg)
        {
            System.IO.File.WriteAllText(@"C:\Error.txt", msg);
        }
    } 
    
    #endregion
}
