﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOLID.DependencyInversionPrinciple
{
    /// <summary>
    /// This Program demonstrates Dependancy Inversion Principle
    /// Inverting Dependacy of class to other or to a calling class which will create object of parent class
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ICustomer Iold = new Customer(new FileErrorHandler());

            IRead Inew = new Customer(new DbErrorHandler());
            

        }
    }

    /// <summary>
    /// Interface for Inquiry customers
    /// </summary>
    public interface IInquiry
    {
        double CalculateDiscount();
    }

    /// <summary>
    /// Interface for actual Customers
    /// </summary>
    public interface ICustomer : IInquiry
    {
        void Add();
    }

    public interface IRead : ICustomer
    {
        void Read();
    }

    /// <summary>
    /// Customer class
    /// </summary>
    public class Customer : ICustomer, IRead // Simple Closed Customer Class
    {

        private IErrorHandler IErr;
        #region Properties

        //private int _customerType;

        //public int CustmerType
        //{
        //    get { return _customerType; }
        //    set { _customerType = value; }
        //}

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public Customer(IErrorHandler i)
        {
            // This is after applying Dependanct Inversion Principle
            // Here Dependancy of creating ErrorHandlers has been 
            // inverted or delegated to main() fuunction
            IErr = i;

            // FOLLOWING WAY IS WRONG WHICH BREAKS SRP
            //int config = 0;  //0 for FileError, 1 for DBError

            //if(config == 0)
            //{
            //    IErr = new FileErrorHandler();
            //}
            //else if(config == 1)
            //{
            //    IErr = new DbErrorHandler();
            //}

        }

        public Customer()
        {

        }

        /// <summary>
        /// Calculates Discount
        /// Method has been marked as Virtual in order to keep it open for modification in other class
        /// </summary>
        /// <returns></returns>
        public virtual double CalculateDiscount()
        {
            //Commented after step 5 below
            //if (CustmerType == 1) // Gold Customer
            //{
            //    return 10;
            //}
            //else // Silver Customer
            //{
            //    return 5;
            //}

            //1. Here the problem is maintaining this class, if tomorrow we need to add bronze customer, we will need to modify the if condition here.
            //2. To Solve this problem we need to use "Extension Class" this class will help use to maintain customerType in this way we wont require to modify customer class each time. 
            //3. This is how we apply OCP
            //4. i.e. Close This Customer Class for Modification but it can be extended. 
            //5. Modification doesn't mean fixing bugs, modification here means adding new functionality (like adding new customer type)
            // lets comment above if-else block


            return 0;


        }

        /// <summary>
        /// Add Customer
        /// </summary>
        public virtual void Add()
        {
            try
            {
                // Adds the customer to database...
                Console.WriteLine("Record Added to database");
            }
            catch (Exception ex)
            {
                //here customer class is doing extra task of writing logs which should be the responsibility of logger class
                // To solve this problem and to follow SRP, we need to create another class which has responsibility of logging

                //System.IO.File.WriteAllText(@"C:\Error.txt",ex.ToString());
                IErr.HandleError(ex.ToString());
            }

        }




        public void Read()
        {
            Console.WriteLine("Read record sucsessful...");
        }
    }



    /// <summary>
    /// Extension of Customer class using inheritance
    /// </summary>
    public class GoldCustomer : Customer // this class is open for extension
    {
        public override double CalculateDiscount()
        {
            return base.CalculateDiscount() + 10;
        }
    }

    /// <summary>
    /// Extension of Customer class using inheritance
    /// </summary>
    public class SilverCustomer : Customer
    {
        
        public override double CalculateDiscount()
        {
            return base.CalculateDiscount() + 5;

        }
    }

    /// <summary>
    /// Extension of Customer class using inheritance
    /// </summary>
    public class InquiryCustomer : IInquiry
    {
        public double CalculateDiscount()
        {
            return 2;
        }
    }



    public interface IErrorHandler
    {
        void HandleError();
        void HandleError(string message);
    }
    /// <summary>
    /// This class handle error handling
    /// </summary>
    public class FileErrorHandler : IErrorHandler
    {
        /// <summary>
        /// 
        /// </summary>
        public FileErrorHandler()
        {

        }

        public void HandleError()
        {

        }

        /// <summary>
        /// This method now handles error
        /// </summary>
        /// <param name="msg"></param>
        public void HandleError(string msg)
        {
            System.IO.File.WriteAllText(@"C:\Error.txt", msg);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class DbErrorHandler : IErrorHandler
    {
        public DbErrorHandler()
        {

        }
        
        public void HandleError()
        {
            Console.WriteLine("Handles DB Errors...");
        }

        public void HandleError(string message)
        {
            Console.WriteLine(message);
        }
    }
}
